[[_TOC_]]
# projetDW2

Membres du groupe :
- MAILLARD WILLIAM
- ZEGHIDI HEDI
- JARMOUNI TAOUFIK
- AYO Manuela

## Introduction

### Sujet

Création d'un document partagé en java avec 1 serveur , 
1 client lourd (swing) et 1 client léger (java-script).

## Type de document

### Descriptions

### Fonctionnalitées
Nous avons choisi de faire une application d'édition collaborative 
d'un fichier texte, avec les fonctionnalités suivantes :

- authentification des utilisateurs
- choix de la taille, couleur, type de font
- centrer du texte
- souligner, mettre en gras ou en italique
- insérer une image (de dimension convenable)

- lors de l'édition collaborative
	- on identifie ce qu'est en train de taper l'utilisateur en surlignant d'une couleur unique permettant de l'identifier
	- tchatche volatile permettant de discuter
	
- action undo/redo grâce a des classes ComandeXXX créées pour chaque entré de l'utilisateur et stocké dans une pile

- droits sur les documents:
	- droit en lecture/écriture
	- un propriétaire, un groupe (optionnel)
	- ajout/suppression/modification d'accès à un document/groupe
	
- commentaire persistant associé à un passage du texte surligné

## Documentation interne

+ [moddels](src/main/java/fr/dw2/models/README.md)
+ [views](src/main/java/fr/dw2/views/README.md)
+ [controllers](src/main/java/fr/dw2/controllers/README.md)


## Organisation

Dans un premier temps nous allons nous consacrer à l'implémentation de nos différents modèles, 
de la création de la BDD et de la réalisation de nos connexions réseaux.
Voici la répartition des tâches pour cette première étape :
- Manuela :
 	- servlet connexion / acceuil
      - page de connexion (formulaire)

- Hedi :  implémentation des modèles suivant
	- documents
	- pages
	- sequences

- Taoufik :  
	- construction des tables de la BDD
	- classe java pour la connexion  à la BDD

- William : 
 	- connexion TCP / webSocket
	- page connexion swing
	- aide implémentations modèles

Dans un second temps, nous allons rassembler le travail fait 
précédemment pour construire nôtre éditeur de document.
Durant cette étape nous allons réaliser la vue basique des 
clients et leurs communication au serveur. 
Puis une fois cela fonctionnel nous pourrons nous consacrer 
à améliorer l'interface utilisateur et ajouter des 
fonctionnalités à notre éditeur.

## Déploiment

*à_venir*
