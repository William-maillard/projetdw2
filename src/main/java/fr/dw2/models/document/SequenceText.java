package fr.dw2.models.document;


import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class SequenceText extends Sequence {
	private static final long serialVersionUID = 3582032386765928225L;
	private List<Character> content = new ArrayList<Character>(100);
	@SuppressWarnings("unused")
	private String fontName, fontColor, fontDecorated;
	private int size;
	
	/** the maximum number contain in one line */
	private int numberOfCharcacterPerLine = 100;
	/* the number of lines of  the sequence */
	private int nbLines = 1;
	/** the space between two lines */
	private int underline = 10;
	private int characterSize = 10;
	
	public SequenceText(int num) {
		super(num);
	}
	
	public void addCharacter(char c) {
		content.add(c);
		updateLines();
	}
	protected void removeCharacter() {
		this.removeCharacter(content.size()-1);
	}
	protected void removeCharacter(int offset) {
		try{
			content.remove(offset);
			updateLines();
			
		}catch(IndexOutOfBoundsException e) {
			return;
		}
	}
	
	private void updateLines() {
		nbLines = content.size() / numberOfCharcacterPerLine + 1;
	}
	
	
	@Override
	public String toString() {
		String s="";
		int i=0;
		
		for(Character c : content) {
			s += c;
			i++;
			if(i > numberOfCharcacterPerLine) {
				i = 0;
				s+="\n";
			}
		}
		return s;
	}
	
	/**
	 * Display the current Sequence beginning at the given line.
	 * @param g
	 */
	public void displaySwing(Graphics g, int x, int y) {
		String s = this.toString();
		boolean isDisplayed = false;
		int begin = 0;
		int end = (content.size() > numberOfCharcacterPerLine) ? numberOfCharcacterPerLine : content.size();
		
		
		/* -- draw a border -- */
		g.setColor(Color.red);
		g.drawRect(x-2, y-2, 
				numberOfCharcacterPerLine * characterSize + 4, 
				this.getHeight() + 4
				);
		y += underline+4;
		
		/* -- draw the sequence -- */
		while(!isDisplayed) 
		{	
			g.setColor(Color.white);
			g.drawString(s.substring(begin, end), x, y);
			
			/* -- update position in the text and in the graphic context --*/
			begin += numberOfCharcacterPerLine;
			end += numberOfCharcacterPerLine;
			y += characterSize + underline;
			
			if(begin >= content.size()) {
				isDisplayed = true;
			}
			else if(end >= content.size()) {
				end = content.size() -1;
			}
		}
	}
	
	
	
	
	
	
	
	
	public int getHeight() {
		return nbLines * (characterSize + underline);
	}
	
	
	/* ----- Generated get & set methods ----- */
	
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	
}
