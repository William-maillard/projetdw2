package fr.dw2.models.document;

/**
 * Represent an array stored in a page.
 * 
 * @author William
 * @see Sequence
 * @see Page
 *
 */
public class SequenceArray extends Sequence {
	private static final long serialVersionUID = 8773541817822991307L;
	private int row, column;

	public SequenceArray(int num) {
		super(num);
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return 0;
	}
}
