package fr.dw2.models.document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import fr.dw2.models.Client;
import fr.dw2.models.command.Command;

public class Document implements Serializable{
	private static final long serialVersionUID = 4392811548360510440L;
	public final String author;
	public final int idAuthor;
	
	//private int id;
	@SuppressWarnings("unused")
	private String title;
	// private Stack<Integer> savedVersion
	private List<Page> pages;
	/** id of the page of the document displaying at the screen */
	@SuppressWarnings("unused")
	private int currentPageNumber;
	/** command make by users to edit this document */
	private Stack<Command> history;
	
	
	public Document(Client author) {
		this.author = author.getPseudo();
		this.idAuthor = author.getId();
		this.currentPageNumber = 0;
		
		pages = new ArrayList<Page>();
		history = new Stack<Command>();
	}

	
	public void editWhithThisCommand(Command command) {
		history.push(command);
		command.execute(); // NB : the Sequence in the document to edit is store as an attribute.
	}
	
	/**
	 * Create the number of the new page.
	 * @return
	 */
	public int createPage() {
		int n = pages.size();
		pages.add(new Page(n));
		return n;
	}
	
	
	public Page getPage(int n) {
		return pages.get(n);
	}
}
