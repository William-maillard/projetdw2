package fr.dw2.models.document;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.concurrent.Semaphore;

import fr.dw2.models.Client;
import fr.dw2.models.command.Command;


/**
 * Represent a fragment of the page which store the data
 * of the document.
 * This fragment can store text, picture or array.
 * @author William
 *
 */
public abstract class Sequence implements Serializable {
	private static final long serialVersionUID = -7093814666220485254L;
	private int currentAuthorID = -1;


	private transient Semaphore takeEdition;
	private final int NUM;
	
	
	protected Sequence(int num) {
		this.NUM = num;
		takeEdition = new Semaphore(1);
	}
	
	
	protected boolean takeEdition(Client author) {
		System.err.println(takeEdition.availablePermits() +" de dispo, idAuthor "+currentAuthorID);
		
		if(takeEdition.availablePermits() == 0) {
			return false;
		}
		
		try 
		{
			System.out.println("Aquire semaphore");
			takeEdition.acquire();
			this.currentAuthorID = author.getId();
			return true;
			
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public void releaseEdition(Client author) {
		if(this.currentAuthorID == author.getId()) {
			takeEdition.release();
			currentAuthorID = -1;
		}
	}
	
	
	public void edit(Client author, Command command) {
		if(this.currentAuthorID != author.getId())
			return;
			
		command.execute();
	}


	public void displaySwing(Graphics g, int x, int y, Color textColor) {
		
		if(this instanceof SequenceText) {
			g.setColor(textColor);
			g.drawString(this.toString(), x, y);
		}
		else if(this instanceof SequencePicture) {
			// TODO 
		}
		else { // SequenceArray
			System.out.println("Array not yet displayed on swing...");
		}

	}
	
	public abstract int getHeight();
	
	/**
	 * @return the currentAuthorID
	 */
	public int getCurrentAuthorID() {
		return currentAuthorID;
	}


	/**
	 * @return the nUM
	 */
	public int getNUM() {
		return NUM;
	}
}
