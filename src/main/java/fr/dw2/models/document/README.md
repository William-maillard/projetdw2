# Representation of a document

A document is represented by the class [Doument](Document.java) which contains all the meta-data on it like the author, the date of creation & last modification...

Then a document is decomposed into [Pages](Page.java) which contain [Sequences](Sequence.java) which store the data of the document (text, picture, array) and their properties (font, color, size, ...). 


In order to edit a document, the user can creating/modifying a sequence by taking its semaphore first. Then he creates [Commandes](Commande.java) in order to edit it.