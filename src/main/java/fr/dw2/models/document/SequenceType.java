package fr.dw2.models.document;

/**
 * Enumerate each type of sequence a age f a document
 * can be composed of.
 * @author William
 *
 */
public enum SequenceType {
	SequenceText,
	SequencePicture,
	SequenceArray,
}
