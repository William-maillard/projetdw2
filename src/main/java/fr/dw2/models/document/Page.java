package fr.dw2.models.document;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import fr.dw2.models.Client;
import fr.dw2.models.ClientTCP;

/**
 * Represent a page of a DOcument.
 * It contains a list of Sequences of different types
 * and some informations on its size.
 * This class also handle the creation/modification of
 * its composed sequences, and its display.
 * 
 * @author William
 * @see SequenceType
 *
 */
@SuppressWarnings("unused")
public class Page implements Serializable {
	static final long serialVersionUID = 7411793177564727729L;
	/** The place of the page in the document she belong */
	private int number;
	/** sequences which compose this page */
	private List<Sequence>content;
	
	
	/* -- display informations -- */
	/** size of the page in pixel */
	private int width = 512, height = 1024;
	/** space between the begining of a sequence and the page's edge */
	private int leftPadding = 10, rightPadding = 10, topPadding = 20, bottomPadding = 20;
	/** orientation of the current page */
	private boolean verticalOrientation = true;	
	

	
	/* ----- Constructors ----- */
	public Page(int number) {
		super();
		
		this.number = number;
		this.content = new LinkedList<Sequence>();
	}
	public Page(int number, boolean verticalOrientation) {
		this(number);
		
		if( !verticalOrientation )
		{
			this.verticalOrientation = false;
			//this.verticalPlaceLeft = 512;
			//this.horizontalPlace = 1024;
		}
	}	
	/* ---------- */
	
	
	/**
	 * A client make a request to modify a sequence on the current page.
	 * @param author 
	 * @param sequenceId
	 * @return <ul>
	 * 			<li>A pointer to the sequence if the client is allowed to modify it</li>
	 * 			<li>null otherwise or if the sequence requested isn't exist</li>
	 * 		   </ul>
	 */
	public Sequence getSequenceToModify(Client author, int sequenceId) {
		try 
		{
			Sequence s = content.get(sequenceId);
			return (s.takeEdition(author)) ? s : null;
			
		}catch(IndexOutOfBoundsException e) {
			System.err.println("The Sequence "+sequenceId+" doesn't exist.");
			return null;
		}
	}	
	
	/**
	 * A new sequence is created on the current page an given to a client
	 * for edition.
	 * This methods take the semaphore on the sequence.
	 * @param author
	 * @param type
	 * @param offset
	 * @return
	 */
	public Sequence createNewSequence(Client author, SequenceType type, int offset) {
		// TODO : find a way to know if there is some place left on the page.
		Sequence newSequence = null;
		
		/* -- create a new Sequence depending on its type
		 *    and add it on the page at the specific offset --*/
		switch(type) {
		
		case SequenceText:
			newSequence = new SequenceText(content.size());
			break;
		case SequencePicture:
			newSequence = new SequencePicture(content.size());
			break;
		case SequenceArray:
			newSequence = new SequenceArray(content.size());
			break;
		}
		newSequence.takeEdition(author);
		//this.moveSequence(newSequence, offset)
		this.content.add(offset, newSequence);
		
		return newSequence;
	}
	
	/**
	 * Create a sequence at the end of all sequence on the page.
	 * @param client
	 * @param sequencetext
	 */
	public Sequence createNewSequence(ClientTCP client, SequenceType sequencetext) {
		return this.createNewSequence(client, sequencetext, this.content.size());		
	}
	
	
	public void displaySwing(Graphics g, Color textColor) {
		
		g.setColor(textColor);
		int x = leftPadding, y = topPadding;
		
		for(Sequence s : content) {
			
			if(s instanceof SequenceText) {
				((SequenceText)s).displaySwing(g, x, y);
				y += ((SequenceText)s).getHeight();	
			}
			else if(s instanceof SequencePicture) {
				// TODO : implement a display function for a SequencePicture
			}
			else { // SequenceArray
				System.out.println("Array not yet displayed on swing...");
			}
			
		}
	}
	
	
	
	
	/* ----- Generated get & set methods ----- */
	
	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	public Sequence getSequence(int n) {
		return content.get(n);
	}

	/**
	 * Use on the client side to update the model
	 * after receiving the new sequence from the server.
	 * @param s
	 */
	public void addSequenceLast(Sequence s) {
		content.add(s);
	}
	
	/**
	 * @return the number of sequence on the current page.
	 */
	public int size() {
		return content.size();
	}
}


















