package fr.dw2.models;

import javax.servlet.http.HttpSession;


/**
 * A client connected from the website.
 * @author William
 *
 * @see Client
 */
public class ClientServlet extends Client{
	private HttpSession session;
	
	public ClientServlet(HttpSession session) {
		this.session = session;
	}

	/**
	 * @return the session
	 */
	public HttpSession getSession() {
		return session;
	}

	@Override
	public void sendModification(Object message) {
		// TODO Auto-generated method stub
		
	}
}