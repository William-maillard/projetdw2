package fr.dw2.models;

import java.util.LinkedList;

import fr.dw2.models.command.Command;
import fr.dw2.models.document.Document;

/**
 * Store a document that can be access by client
 * with a given ID and password.
 * @author William
 *
 */
public class SharedEdition {
	private static int InstanceCounter = 0;
	/** To identify the current session */
	public final int ID;
	/** List of all the client who have access to the session
	 *  THe first of the list is the owner */
	private LinkedList<Client> clients;
	/** The password to join the session */
	private String password; // TODO : maybe hash it ...
	/** The shared document */
	private Document doc;
	
	/**
	 * Create a session for a document 
	 * with the current password 
	 * @param password
	 */
	public SharedEdition(Document doc, String password, Client owner) {
		ID = InstanceCounter++;
		this.doc = doc;
		this.password = password;
		this.clients = new LinkedList<Client>();
		this.clients.add(owner);
	}

	
	/**
	 * Check at the connection if the given password is correct.
	 * @param password
	 * @return  <ul> 
	 * 				<li>true, if the password is correct</li>
	 * 				<li>false, otherwise</li>
	 * 			</ul>
	 */
	public boolean granted(String password) {
		return this.password.equals(password) ? true : false;
	}
	
	
	@Override
	public int hashCode() {
		return this.ID;
	}


	/**
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}


	/**
	 * @return the owner
	 */
	public Client getOwner() {
		return clients.getFirst();
	}
	
	
	public LinkedList<Client> getClients() {
		return clients;
	}


	/**
	 * If the current owner wants to pass his session to 
	 * someone.
	 * @param owner : the current owner
	 * @param newOwner : the owner to set
	 * @return true if the change happen, false otherwise.
	 */
	public boolean setOwner(Client owner, Client newOwner) {
		if(owner.getId() == this.clients.getFirst().getId()) {
			clients.remove(newOwner);
			this.clients.addFirst(newOwner);
			return true;
		}
		return false;
	}
	
	
	public void join(Client client) {
		clients.add(client);
	}
	
	public void quit(Client client) {
		clients.remove(client);
	}
	
	
	public void sendModification(Command commandToExecute) {
		
	}
}
