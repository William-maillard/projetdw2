package fr.dw2.models;

import java.io.IOException;


/**
 * Represent a connected client.
 * This class store information extract from the DB 
 * at the client's connection.
 * 
 * @see ClientTCP
 * @see ClientServlet
 * @see ClientWS
 * 
 * @author William
 */
public abstract class Client { 
	private String mail, pseudo;
	private int id;
	
	public Client() {
		super();
	}
	
	@Deprecated
	public Client(String mail, String pseudo, int id) {
		this.setMail(mail);
		this.setPseudo(pseudo);
		this.setId(id);		
	}

	/**
	 * To send a message at the current client without
	 * having to worry about the protocol used.
	 * @param message to send
	 * @throws IOException
	 */
	public abstract void sendModification(Object message) throws IOException;
	
	
	
	/* ----- Generated get & set methods ----- */
	
	/**
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * @param pseudo the pseudo to set
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
