package fr.dw2.models;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * A client connected from the desktop application.
 * @author William
 *
 */
public class ClientTCP extends Client{
	@SuppressWarnings("unused")
	private static JSONParser jsonParser= new JSONParser(); // old way we used to send JSON as string, now it's as Object
	private Socket socket;
	/** stream to send a panel to a client */
	private ObjectOutputStream out;
	/** stream to receive the data send by the client */
	private ObjectInputStream in;

	
	public ClientTCP(Socket socket) throws IOException {
		this.socket = socket;
		
		/* -- create communication streams -- */
		out = new ObjectOutputStream(socket.getOutputStream());
		in = new ObjectInputStream(socket.getInputStream());
	}
	
	
	/**
	 * Send the message using the socket of this instance.
	 * @param message : an Object to send to the client
	 * @throws IOException
	 */
	public void sendMessage(Object message) throws IOException {
		out.writeObject(message);
	}
	
	/**
	 * @return the receiving message
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public Object receiveMessage() throws ClassNotFoundException, IOException {
		return in.readObject();
	}
	

	/**
	 * wait for a request from the current client.
	 * @return the JSON request of the client received 
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws ClassNotFoundException 
	 */
	public JSONObject waitForRequest() throws IOException, ParseException, ClassNotFoundException {	
		/* -- First get the size of the message (block here until there is a message in the stream) -- */
		//int size = in.read();
		
		/* -- Then take the message and cast it to a JSONObject -- 
		byte[] rawData = new byte[size]; 
		in.read(rawData);
		String message = new String(rawData);	
		System.out.println("(debug message l64 ClientTCP)Message received : "+message);
		
		return (JSONObject)jsonParser.parse(message);*/
		return (JSONObject) in.readObject();
	}

	
	/**
	 * Close streams and socket of a client
	 */
	public void close() {
		try {
			in.close();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	@Override
	public void sendModification(Object message) throws IOException {
		out.writeObject(message);		
	}
}
