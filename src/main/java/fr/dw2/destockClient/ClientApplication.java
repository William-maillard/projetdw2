package fr.dw2.destockClient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.json.simple.JSONObject;

import fr.dw2.controllers.server.ServerConfigurationTCP;
import fr.dw2.models.command.Command;
import fr.dw2.models.document.Document;
import fr.dw2.models.document.Sequence;
import fr.dw2.views.heavyClient.DocumentPanel;
import fr.dw2.views.heavyClient.PanelWithEventToRegister;

/**
 * It's the Frame of the desktop application.
 * It's handle the exchange with the server and set the panel received 
 * from the server.
 * @author William
 */
public class ClientApplication extends JFrame implements ServerConfigurationTCP{
	private static final long serialVersionUID = 7499791724645129593L;
	/** the screen dimension for center the frame or make a full screen mode */
	private static final Dimension SCREEN_DIM = Toolkit.getDefaultToolkit().getScreenSize();

	
	/* ----- Communication ----- */	
	/** to communicate with the server through a TCP protocol */
	private Socket socket;
	/** stream to retrieve application's panels from the server */
	private ObjectInputStream incomingData;
	/** stream to send Object to the server */
	private ObjectOutputStream sendData;

	
	/* ----- Model's informations ----- */	
	/** document to edit */
	private Document doc;
	/** the number of the page currently display to the user */
	private int numberOfCurrentPage, numberOfCurrentSequence = -1;
	/** the sequence which is being editing by the use */
	private Sequence currentEditingSequence;
	
	
	/* ----- Shared edition informations ----- */
	/** used during a sharedEdition to keep the document update */
	private Thread updating;
	/** if the current page is changed this indicate to the view to redisplay it */
	private boolean CurrentPageNeedsRedisplay = false;
	
	
	
	public ClientApplication() {
		super();
		
		//this.setUndecorated(true);
		//this.setResizable(false);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/* -- server connection through a TCP protocol-- */
		try {
			socket = new Socket(HOST_ADDRESS, PORT);
			
		} catch (IOException e) {
			e.printStackTrace();
			String errorMessage = "Erreur de connexion avec le serveur "+HOST_ADDRESS+" sur le port "+PORT+".";
			System.err.println(errorMessage);
			JOptionPane.showMessageDialog(null, errorMessage);
			System.exit(ABORT);// close the application
		}
		
		
		/* -- The connection has been establish, now it opens streams and retrieves the connection panel -- */
		System.err.println("connecté.");
		try 
		{
			incomingData = new ObjectInputStream(socket.getInputStream());
			sendData = new ObjectOutputStream(socket.getOutputStream());	
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		
		this.waitForPanel();
	}
	
	
	/**
	 * Center the frame on the user's screen.
	 */
	public void center() {
		this.setLocation(
				(int)SCREEN_DIM.getWidth()/2  -  this.getWidth()/2,
				(int)SCREEN_DIM.getHeight()/2 -  this.getHeight()/2
				);
	}
	
	/**
	 * Wait for an object incoming from the server.
	 * @return the incoming object
	 */
	public Object waitForAnswer() {
		Object o = null;
		try {
			o = incomingData.readObject();
		} catch (ClassNotFoundException | IOException e) {
			System.out.println("Error while waiting for a response of the TCP server");
			e.printStackTrace();
			e.getMessage();
		}
		return o;
	}
	
	
	/**
	 * Wait an incoming panel from the server and set it as
	 * the new contentPane.
	 * This methods is blocking until a message is received.
	 */
	public void waitForPanel() {
		// TODO change the protocol to JSON key, Object ?
		System.out.println("Waiting for the server response.");
		try {
			Object panel = waitForAnswer();
			this.setContentPane((JPanel)panel);
			
			if(panel instanceof DocumentPanel) {
				/* -- recover the document model -- */
				doc = (Document)incomingData.readObject();
				this.setNumberOfCurrentPage(0);
				CurrentPageNeedsRedisplay = true;
				//this.setCurrentEditingSequence(doc.getPage(this.numberOfCurrentPage).getSequence(0));
				
			}
			
			/* -- Register the event of the panel received -- */
			((PanelWithEventToRegister)panel).recordEventListeners(this);
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.err.println("Error for the reception");
		}
		
		this.pack();
		this.center();
	}
	
	/**
	 * @param message
	 *
	public void senMessageToServer(JSONObject message) {
		try {
			byte[] messageToSend = message.toJSONString().toString().getBytes("UTF-8");
			System.err.println("Envoie du message : "+message); // debug message
			sendData.write(messageToSend.length); // send the number of bytes of the message
			sendData.write(messageToSend); // then send the message content
			sendData.flush(); // force the stream to push is content

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Something went wrong when I send a message :(");
		}
	}*/
	public boolean sendMessageToServer(Object message) {
		try {
			sendData.writeObject(message);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Something went wrong when I send a message :(");
			return false;
		}
		return true;
	}
	
	

	/**
	 * allow a client to create a session to invite other
	 * clients to join him for editing its current document.
	 * @param password
	 * @return n>= 0 : l'identifiant de la session créé, -1 sinon
	 */
	public int launchSharedSession(String password) {
		try {
			sendData.writeObject(password);
			Integer response = (Integer)incomingData.readObject();
			if(response != -1) {
				waitForUpdate();
			}
			
			return response;
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	
	/**
	 * While a sharedSession launch a thread to wait for updates of the documents
	 * from the server.
	 * @return
	 */
	private void waitForUpdate() {
		updating = new Thread() {
			@Override
			public void run() {
				super.run();
				Object o;

				/* -- wait for a command -- */
				System.err.println("Wait for a command");
				while(!isInterrupted()) {
					try {
						o = incomingData.readObject();
						
						
						if(o instanceof Command) {
							Command command = (Command)o;
							System.out.println("Command received to update the document page "+command.getPageNumber()+" seq "+command.getSequenceNumber());
							/* -- pass the sequence to modify to the command -- */
							command.setSequence(doc.getPage(command.getPageNumber()).getSequence(command.getSequenceNumber()));
							/* -- apply the command -- */
							doc.editWhithThisCommand(command);
							
							/* -- check if the user's pane needs to be update -- */
							if(true/*numberOfCurrentPage == command.getPageNumber()*/) {
								setCurrentPageNeedsRedisplay(true);
								ClientApplication.this.getContentPane().repaint();
							}
						}
						else if(o instanceof JSONObject) { 
							// TODO : make a function to avoid redundancy
							JSONObject message = (JSONObject)o;
							// receiving a sequence after asking for take edition
							System.out.println("361 : sequence reçu");
							Sequence sequenceReceived = (Sequence)message.get("SEQUENCE");
							doc.getPage(numberOfCurrentPage).addSequenceLast(sequenceReceived);

							if((boolean)message.get("EDIT")) {
								System.out.println("Sequence reçue, on a l'édition dessus : "+sequenceReceived.getNUM());
								ClientApplication.this.currentEditingSequence = sequenceReceived;
								ClientApplication.this.numberOfCurrentSequence = sequenceReceived.getNUM();
							}

						}
						else {
							System.out.println("Object received");
						}
						
					} catch (ClassNotFoundException | IOException e) {
						e.printStackTrace();
					}
				}
			}
		};
		updating.start();
	}
	
	
	/**
	 * Stop the Thread which wait for command to keep the current 
	 * document updated.
	 */
	public void stopWaitForUpadate() {
		updating.interrupt();
		updating = null;
	}
	
	/**
	 * Launch a thread for receiving command in background,
	 * if it already exist do nothing.
	 */
	public void startWaitForUpdate() {
		if(updating != null) return;
		waitForUpdate();
	}
	
	
	@SuppressWarnings("unchecked")
	public void exitEditionMode() {
		// notify the server 
		JSONObject json = new JSONObject();
		json.put("TYPE", "REQUEST_QUIT");			
		try {
			sendData.writeObject(json);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		// stop the background thread
		if(updating != null) stopWaitForUpadate();
		
		// wait for a new Pane to display
		waitForPanel();		
	}
	
	/**
	 * Ask to the server if the client can modify the sequence 
	 * ont the current page.
	 * @param n
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public boolean askForEditSequence(int n) {
		JSONObject json = new JSONObject();		
		json.put("TYPE", "REQUEST_TAKE_EDITION");
		json.put("PAGE", this.numberOfCurrentPage);
		json.put("value", n);
		json.put("release", this.getNumberOfCurrentSequence());
		
		try {
			sendData.writeObject(json);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		// wait for the response :
		System.err.println("askForeditSequence() : wait the server's response");
		String response = (String) this.waitForAnswer();
		System.out.println("(ClientApplication 322) demande d'édition sur la "+n+" sequence réponse "+response);
		
		// update attributes of the application
		if(response.equals("T")) {
			this.numberOfCurrentSequence = n;
			this.currentEditingSequence = doc.getPage(numberOfCurrentPage).getSequence(numberOfCurrentSequence);
			System.out.println("Séquence d'édition mise à jour "+n);
			return true;
		} 

		System.out.println("édition refusé.");
		return false;
	}
	
	
	
	
	/**
	 * Ask the server to create a sequence at the bottom free location
	 *  on the current displaying page.
	 */
	@SuppressWarnings("unchecked")
	public boolean requestSequenceCreation() {
		JSONObject json = new JSONObject();
		json.put("TYPE", "REQUEST_CREATE_SEQUENCE_TEXT");
		json.put("PAGE", numberOfCurrentPage);
		
		
		try {
			sendData.writeObject(json);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		if(this.updating == null) { // if a share edition is ongoing there is a thread handling incomes.
			// response receive the new sequence
			Object o = waitForAnswer();
			if(o instanceof JSONObject) {
				// TODO make a function to avoid reduncdance whith the block in updating thread.
				JSONObject message = (JSONObject)o;
				// receiving a sequence after asking for take edition
				System.out.println("361 : sequence reçu");
				Sequence sequenceReceived = (Sequence)message.get("SEQUENCE");
				doc.getPage(numberOfCurrentPage).addSequenceLast(sequenceReceived);

				if((boolean)message.get("EDIT")) {
					System.out.println("Sequence reçue, on a l'édition dessus");
					ClientApplication.this.currentEditingSequence = sequenceReceived;
					ClientApplication.this.numberOfCurrentSequence = sequenceReceived.getNUM();
				}

			}
			else {
				System.err.println("370 clienApp ONI received ! "+o);
			}
		}
		
		return true;
	}
	
	
	
	/**
	 * Create a sequence on the current page
	 * @param offset 
	 */
	@Deprecated
	public void createNewSequence(int offset) {
		// TODO :
	}
	public void modificationOfSequence(int offset) {
		// TODO :
	}


	public void redisplayCurrentPage(Graphics g, Color color) {
		//System.out.println("ClientApp redisplay page "+numberOfCurrentPage);
		this.doc.getPage(numberOfCurrentPage).displaySwing(g, color);
	}

	/**
	 * The client click on the panel, this method look if he click on a Sequence
	 * and he can edit it, or create a new Sequence at the end of the doc.
	 * @param y
	 */
	public void editSequenceAtHeight(int y) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	/* ----- Generated get & set methods ----- */
	

	/**
	 * @return the currentEditingSequence
	 */
	public Sequence getCurrentEditingSequence() {
		return currentEditingSequence;
	}
	/**
	 * @return the numberOfCurrentPage
	 */
	public int getNumberOfCurrentPage() {
		return numberOfCurrentPage;
	}
	/**
	 * @param numberOfCurrentPage the numberOfCurrentPage to set
	 */
	public void setNumberOfCurrentPage(int numberOfCurrentPage) {
		this.numberOfCurrentPage = numberOfCurrentPage;
	}
	
	public void setCurrentEditingSequence(Sequence sequence) {
		this.currentEditingSequence = sequence;
	}
	/**
	 * @return the currentPageNeedsRedisplay
	 */
	public boolean isCurrentPageNeedsRedisplay() {
		return CurrentPageNeedsRedisplay;
	}
	/**
	 * @param currentPageNeedsRedisplay the currentPageNeedsRedisplay to set
	 */
	public void setCurrentPageNeedsRedisplay(boolean currentPageNeedsRedisplay) {
		CurrentPageNeedsRedisplay = currentPageNeedsRedisplay;
	}
	/**
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}
	/**
	 * @return the numberOfCurrentSequence
	 */
	public int getNumberOfCurrentSequence() {
		return numberOfCurrentSequence;
	}

}
