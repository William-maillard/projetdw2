package fr.dw2.views.heavyClient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.json.simple.JSONObject;

import fr.dw2.destockClient.ClientApplication;

/**
 * View of the connection's form of the desktop application.
 * @author William
 *
 */
public class ConnectionPanel extends JPanel implements PanelWithEventToRegister{
	private static final long serialVersionUID = 3253028371060067921L;
	public static final int PSEUDO_MIN_LENGTH = 6, PW_MIN_LENGTH=5; // TODO put this in a config file or an interface ...?
	private static final int WIDTH_TEXTFIELDS = 50;
	private JButton submit;
	private JTextField loginInput, passwordInput;
	
	
	public ConnectionPanel() {
		super();
		
		
		/* -- panel's set up -- */
		this.setLayout(null);
		this.setPreferredSize(new Dimension(512, 512));
		this.setBackground(Color.gray.brighter());
		this.setLayout(null);
		
		
		/* -- components' creation -- */
		JLabel loginLabel, passwordLabel;
		JButton submit;
		try {
			BufferedImage spriteSheet = ImageIO.read(getClass().getClassLoader().getResource("login.png"));
			loginLabel = new JLabel(new ImageIcon(spriteSheet.getSubimage(0, 32, 32, 32)));
			passwordLabel = new JLabel(new ImageIcon(spriteSheet.getSubimage(32, 64, 32, 32)));
			submit = new JButton("valider", new ImageIcon(spriteSheet.getSubimage(0, 64, 32, 32)));
		} catch (IOException e) {
			System.err.println("Error while loading an image");
			// TODO : use a .properties file and bundle class
			loginLabel = new JLabel("connexion");
			passwordLabel = new JLabel("MDP");
			submit = new JButton("valider");
		}
		JTextField loginInput = new JTextField(WIDTH_TEXTFIELDS);
		JTextField passwordInput = new JPasswordField(WIDTH_TEXTFIELDS);

		
		/* -- set components' location  -- */
		loginLabel.setLabelFor(loginInput);
		passwordLabel.setLabelFor(passwordInput);
		
		loginInput.setBounds(156, 100, 200, 50);
		passwordInput.setBounds(156, 175, 200, 50);
		submit.setBounds(156, 250, 200, 25);
		
		// labels
		Point p;
		p = loginInput.getLocation();
		loginLabel.setBounds((int)(p.getX()-32), (int)p.getY()+9, 32, 32);
		p = passwordInput.getLocation();
		passwordLabel.setBounds((int)(p.getX()-32), (int)p.getY()+9, 32, 32);
		
		
		/* -- adding components to the panel --*/
		this.add(loginInput);
		this.add(loginLabel);
		this.add(passwordInput);
		this.add(passwordLabel);
		this.add(submit);
		
		
		// validation button
		submit.setBackground(Color.green.darker());
		submit.setFocusPainted(false);
		this.submit = submit;
		this.loginInput = loginInput;
		this.passwordInput = passwordInput;
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
	}
	
	
	@Override
	public void recordEventListeners(ClientApplication clientApp) {
		submit.addActionListener(new ActionListener() {	
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				String pseudo = loginInput.getText();
				String password = passwordInput.getText();

				/*if(pseudo.length() < PSEUDO_MIN_LENGTH  ||  !pseudo.contains("@")  ||  password.length() < PW_MIN_LENGTH) {
					// TODO : error message
					System.err.println("Les champs sont vide");
					return;
				}*/		
		
				/* -- preparation of the message -- */
				JSONObject message = new JSONObject();
				message.put("type", "connexion"); // TODO : faire une enum des types de messages, type -> entier 
				message.put("login", pseudo);
				message.put("password", password);

				/* -- sending -- */
				clientApp.sendMessageToServer(message);

				/* -- waiting for response -- */
				clientApp.waitForPanel();

			}
		});
	}
}
