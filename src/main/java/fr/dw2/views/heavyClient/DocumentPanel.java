package fr.dw2.views.heavyClient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import fr.dw2.destockClient.ClientApplication;
import fr.dw2.models.command.Command;
import fr.dw2.models.command.CommandTypeCharacter;
import fr.dw2.models.document.SequenceText;

/**
 * View of the document on the desktop application.
 * @author William
 *
 */
public class DocumentPanel extends JPanel implements PanelWithEventToRegister {
	private static final long serialVersionUID = 9032051112733632639L;
	private JToolBar menu;
	private JButton createSharedSession, createSequenceText, exit;
	
	@SuppressWarnings("unused")
	private static int cursorWidth = 2, cursorHeight = 6;
	private int padding = 20;
	private String c;
	private int characterWidth =5, characterHeight = 5;
	private int horizontalOffset = padding, verticalOffset = padding+characterHeight;
	private Color textColor = Color.white;

	/** use when we redisplay the panel */
	private boolean typed=false;
	/** A controller which handle communications with the server and local model update */
	private ClientApplication clientApp;
	/** To identify the sharedEdition used by this view */
	private int idEdition = -1;
	
	
	
	public DocumentPanel() {
		super();
		
		/* -- panel's set up -- */
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(512, 1024));
		this.setBackground(Color.BLACK.brighter());
		this.setLayout(new BorderLayout());
		
		/* -- Create ToolBar and its buttons -- */
		menu = new JToolBar();
		this.add(menu, BorderLayout.WEST);
		menu.add(new JButton("Do nothing...."));
		
		createSharedSession = new JButton("édition partagée");
		createSharedSession.setBackground(Color.green);
		menu.add(createSharedSession);
		
		createSequenceText = new JButton("Créer zone de texte");
		createSequenceText.setBackground(Color.gray);
		menu.add(createSequenceText);
		
		
		exit = new JButton("Quitter");
		exit.setBackground(Color.red);
		menu.add(exit);
		
		
		/* -- components' creation -- */
	}
	
	public DocumentPanel(int idEdition) {
		this();
		this.idEdition = idEdition;
		
		createSharedSession.setEnabled(false);
		createSharedSession.setBackground(Color.red);
		createSharedSession.setText("numéro "+idEdition);	
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		//Graphics2D g2D = (Graphics2D)g;
		
		if(typed) {
			g.setColor(textColor);
			g.drawString(c, horizontalOffset, verticalOffset+characterHeight);
			horizontalOffset += characterWidth;
			if(this.getWidth() - padding <= horizontalOffset) { // end of line
				horizontalOffset = padding;
				verticalOffset += characterHeight;
				// TODO handle the end of page.
				
			}
			typed = false;
		}
		
		/* -- drawing the editing sequence --*/
		if(true/*clientApp.isCurrentPageNeedsRedisplay()*/) {
			//clientApp.setCurrentPageNeedsRedisplay(false);
			clientApp.redisplayCurrentPage(g, textColor);
		}
		/*else if(clientApp.getCurrentEditingSequence() != null){
			clientApp.getCurrentEditingSequence().displaySwing(g, 5, 20, textColor);
			System.out.println("Display Sequence");
		}*/
		
		/* -- drawing the cursor -- */
		g.setColor(textColor);
		int x = horizontalOffset + characterWidth;
		g.drawLine(x, verticalOffset, 
				x, verticalOffset+cursorHeight
				);
	}
	
	
	@Override
	public void recordEventListeners(ClientApplication clientApp) {
		this.requestFocus();
		this.clientApp = clientApp;
		
		
		this.addKeyListener(new KeyListener() {
			/**
			 * Key Event functionalities :
			 * 	1) create a command
			 *  2) execute command
			 * 	3) send the command
			 *  4) display
			 */
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO : check if it is a valid character by the unit-code
				// Is the user editing a text sequence ?
				if(clientApp.getCurrentEditingSequence() instanceof SequenceText) {
					Command command = new CommandTypeCharacter(
							e.getKeyChar(), 
							clientApp.getNumberOfCurrentPage(), 
							clientApp.getCurrentEditingSequence()					
							);
					command.execute();
					// TODO : remove debug line
					System.out.println("Command send for page "
							+clientApp.getNumberOfCurrentPage()+" command page "+command.getPageNumber()
							+" and sequence"+clientApp.getCurrentEditingSequence().getNUM() 
							+ " command seq "+command.getSequenceNumber()
							);
					
					/* here set the num of sequence because a weird thing append :
					 * if we don't command.getSequenceNumber() return always 0 in the server side
					 * But this didn't happen if we set that attribute here.*/
					command.setSequenceNumber(clientApp.getCurrentEditingSequence().getNUM());
					clientApp.sendMessageToServer(command);
				}
				c = ""+e.getKeyChar();
				
				typed=true;
				DocumentPanel.this.repaint();
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		createSharedSession.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String password = JOptionPane.showInputDialog (null, "Entrer un mdp (5 lettres min) :");
				//if(password.length() < 5) return;
				
				int id = clientApp.launchSharedSession(password);
				if(id == -1) return;
				
				createSharedSession.setEnabled(false);
				createSharedSession.setBackground(Color.red);
				createSharedSession.setText("numéro "+id);	
				
				DocumentPanel.this.requestFocus();
			}
		});
		
		
		createSequenceText.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				DocumentPanel.this.clientApp.requestSequenceCreation();
				
			}
		});
		
		
		exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				DocumentPanel.this.clientApp.exitEditionMode();
			}
		});
		
		
	
		this.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// The user click on a Sequence ?
				// TODO DocumentPanel.this.clientApp.editSequenceAtHeight(e.getY());
				// Pour l'instant demande l'édition de la première séquence.
				/*if(DocumentPanel.this.clientApp.getCurrentEditingSequence() == null) {
					System.out.println("ask for take edition");
					DocumentPanel.this.clientApp.askForEditSequence(0);
				}*/
			}
		});
		
	}
}
