package fr.dw2.views.heavyClient;

import fr.dw2.destockClient.ClientApplication;

/**
 * Represent a panel with event send throught a TCP socket
 * from the server to the client.
 * 
 * @author William
 *
 */
public interface PanelWithEventToRegister {
	
	/**
	 * Once the panel received, this methods must be called to record the events of
	 * panel's elements to swing.
	 * @param clientApp 
	 */
	public void recordEventListeners(ClientApplication clientApp);
}
