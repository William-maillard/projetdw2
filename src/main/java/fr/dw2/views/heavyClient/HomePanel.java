package fr.dw2.views.heavyClient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.json.simple.JSONObject;

import fr.dw2.destockClient.ClientApplication;

/**
 * Home view of the user desktop application which contains : 
 * <ul>
 * 		<li>a button to create a new document</li>
 *      <li>a button to consult user's documents</li>
 * </ul>
 *
 * @author William
 */
public class HomePanel extends JPanel implements PanelWithEventToRegister {
	private static final long serialVersionUID = -6899509349995495547L;
	private JButton createDocument, consultDocument, joinEdition;

	public HomePanel() {
		super();


		/* -- panel's set up -- */
		this.setLayout(null);
		this.setPreferredSize(new Dimension(512, 512));
		//this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		this.setBackground(Color.blue.darker());
		this.setLayout(null);

		/* -- components' creation-- */
		createDocument = new JButton("nouveau document");
		consultDocument = new JButton("mes documents");
		joinEdition = new JButton("rejoindre session");

		createDocument.setBounds(156, 100, 200, 50);
		consultDocument.setBounds(156, 175, 200, 50);
		joinEdition.setBounds(156, 250, 200, 50);

		/* -- adding components --*/
		this.add(createDocument);
		this.add(consultDocument);
		this.add(joinEdition);
	}



	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

	}


	@Override
	public void recordEventListeners(ClientApplication clientApp) {
		
		createDocument.addActionListener(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				/* -- preparation of the message-- */
				JSONObject message = new JSONObject();
				message.put("type", "request"); 
				message.put("value", "newDocument");


				/* -- sending -- */
				clientApp.sendMessageToServer(message);

				/* -- waiting for response -- */
				clientApp.waitForPanel();
			}
		});
		

		consultDocument.addActionListener(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				/* -- preparation of the message -- */
				JSONObject message = new JSONObject();
				message.put("type", "myDocuments"); 

				/* -- sending -- */
				clientApp.sendMessageToServer(message);

				/* -- waiting for response -- */
				clientApp.waitForPanel();

			}
		});
		
		joinEdition.addActionListener(new ActionListener() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				int id;
				String response = JOptionPane.showInputDialog (null, "Entrer le numero de session que vous voulez rejoindre :");

				try {
					id = Integer.parseInt(response);
				} catch(NumberFormatException exception) {
					JOptionPane.showMessageDialog(null, "Nombre incorrect");
					return;
				}
				response = JOptionPane.showInputDialog (null, "Entrer le mot de passe de la session "+id+" :");
				
				/* -- preparation of the message-- */
				JSONObject message = new JSONObject();
				message.put("type", "request"); 
				message.put("value", "joinEdition");
				message.put("id", Integer.toString(id));
				message.put("password", response);


				/* -- sending -- */
				clientApp.sendMessageToServer(message);

				/* -- waiting for response -- */
				Object o = clientApp.waitForAnswer();
				if(o.equals("NO")) {
					JOptionPane.showMessageDialog(null, "Erreur de mdp ou l'édition demandé n'existe pas.");
					System.out.println("SharedEdition reffusé");
				}
				else {
					System.out.println("Shared edition accepté !");
					/* -- received a JPanel and a document -- */
					clientApp.waitForPanel();
					clientApp.startWaitForUpdate();
				}
			}
		});
	}
}
