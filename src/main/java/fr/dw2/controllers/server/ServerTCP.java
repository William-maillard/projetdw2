package fr.dw2.controllers.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Create a TCP server.
 * Launch a thread which accepting client until is interrupted.
 * Launch a Thread for each client.
 */
public class ServerTCP extends Thread implements ServerConfigurationTCP {	
	private ServerSocket server;
	/** thread launched to listen the connection of new clients */
	private Thread waitForClient;
	/** list of connected clients */
	private ArrayList<ThreadClientTCP> listOfConnectedClients = new ArrayList<ThreadClientTCP>(10);
	
	
	/**
	 * Create a TCP server and launch a Thread to accept clients' connections
	 */
	public ServerTCP() {
		try
		{
			server = new ServerSocket(PORT);
			
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Error, the server couldn't be launched");
		}

		waitForClient = new Thread() {
			@Override
			public void run() {
				super.run();
				Socket newClient;
				
				while(!this.isInterrupted())
				{
					try 
					{
						newClient = server.accept();
						ThreadClientTCP clientThread = new ThreadClientTCP(newClient);
						listOfConnectedClients.add(clientThread);
						clientThread.start();
						
					} catch (IOException e) {
						e.printStackTrace();
						System.err.println("Something went wrong during the bidding with a client");				
					}
				}
			}
		};

		waitForClient.start();
	}

	/**
	 * To allow the server to accept incoming connections.
	 */
	public void acceptingClient() { waitForClient.start(); }
	
	/**
	 * To disallow the server to accept incoming connections.
	 */
	public void stopAcceptingClient() {
		waitForClient.interrupt();
		System.err.println("The server have been stopped.");
	}
	
	/**
	 * Interrupts all clients' thread and stop the server.
	 */
	public void shutDown() {
		for(Thread client : listOfConnectedClients)
			client.interrupt();		
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
