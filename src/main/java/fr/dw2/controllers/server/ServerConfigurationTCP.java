package fr.dw2.controllers.server;

/**
 * Store the server's address and port to make them easy to change.
 * @author William
 */
public interface ServerConfigurationTCP {
	String HOST_ADDRESS = "localhost";
	int PORT = 8086;
}
