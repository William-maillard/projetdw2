package fr.dw2.controllers.server;


import java.util.HashMap;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import fr.dw2.models.ClientWS;

import javax.websocket.OnMessage;
import javax.websocket.OnOpen;

// TODO : think about a JSON protocol to exchange data.
@ServerEndpoint("/serverWS")
public class ServerWS {
	private static HashMap<Session, ClientWS>connectedClients = new HashMap<Session, ClientWS>();
	
	@OnOpen
	public void open(Session session) {
		System.out.println("connexion du client WS");
	}
	@OnClose
	public void close(Session session, CloseReason reason) {
		// TODO
	}
	@OnMessage
	public void message(Session session, String message) {
		System.out.println("message reçu : "+message);
	}
	@OnError 
	public void error(Session session, Throwable t) {
		// TODO 
	}
}
