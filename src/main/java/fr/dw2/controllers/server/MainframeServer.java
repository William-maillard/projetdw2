package fr.dw2.controllers.server;

import java.util.HashMap;
import java.util.Map;

import fr.dw2.models.Client;
import fr.dw2.models.ClientServlet;
import fr.dw2.models.SharedEdition;
import fr.dw2.models.document.Document;

/**
 * It's contains all the data available on the server side
 * such as connected client (through the web site or the application),
 * collaborative edition session, ...
 * @author William
 *
 */
public class MainframeServer {	
	/** The instance of the current launch mainframe server */
	public static final MainframeServer instance = new MainframeServer();
	/** store each connected client */
	private static Map<Integer, Client> connectedClient;
	@SuppressWarnings("unused")
	private /*final*/ ServerTCP serverTCP ;//= new ServerTCP(); // TODO : uncomment this line and don't create a server in the main class
	/** To store all open sharedSession */
	private Map<Integer, SharedEdition> sharedEditions;
	
	
	
	/** only one instance of this class is made */
	private MainframeServer() {
		connectedClient = new HashMap<Integer, Client>();
		sharedEditions = new HashMap<Integer, SharedEdition>();
	}
	
	
	/**
	 * Called by a server when it accepts a connection.
	 * @param client
	 */
	protected void addClient(Client client) {
		connectedClient.put(client.getId(), client);
	}
	
	public void addClient(ClientServlet client) {
		addClient((Client)client);
		
	}
	
	/**
	 * Called by a server when a client is disconnecting
	 * @param client
	 */
	protected void removeClient(Client client) {
		connectedClient.remove(client.getId());
	}
	
	
	
	/**
	 * Create an edition with a document that can be access
	 * by all granted participant.
	 * @param doc : the document to share
	 * @param password : to join the session
	 * @param owner : the client who is creating the session
	 * @return the new session if it was successful, null otherwise.
	 */
	public SharedEdition createSharedEdition(Document doc, String password, Client owner) {
		SharedEdition edition = new SharedEdition(doc, password, owner);
		this.sharedEditions.put(edition.hashCode(), edition);
		return edition;
	}
	
	
	/**
	 * Get access to a document's edition
	 * @param id : identify the session
	 * @param password : to get access to it
	 * @param client : the client asking to get access
	 * @return 	<ul>
	 * 				<li>the SharedEdition if the password is correct</li>
	 *				 <li>null, otherwise</li>
	 * 			</ul>
	 */
	public SharedEdition getsharedEdition(int id, String password, Client client) {
		SharedEdition session = this.sharedEditions.get(id);
		return session == null ? null : session.granted(password) ? session : null;
	}
	
}









































