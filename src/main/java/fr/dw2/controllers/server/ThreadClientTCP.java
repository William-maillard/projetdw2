package fr.dw2.controllers.server;


import java.io.IOException;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import fr.dw2.models.Client;
import fr.dw2.models.ClientTCP;
import fr.dw2.models.RequestType;
import fr.dw2.models.SharedEdition;
import fr.dw2.models.command.Command;
import fr.dw2.models.document.Document;
import fr.dw2.models.document.Sequence;
import fr.dw2.models.document.SequenceType;
import fr.dw2.views.heavyClient.ConnectionPanel;
import fr.dw2.views.heavyClient.DocumentPanel;
import fr.dw2.views.heavyClient.HomePanel;


/**
 * This class handle the exchange between a client and the server
 * Through a TCP channel.
 * @author William
 *
 * @see ClientTCP 
 */
public class ThreadClientTCP extends Thread {
	private static int InstanceCounter = 0;
	public final int ID;
	/** model which store the data taking in the DB and the socket */
	private ClientTCP client;
	
	/** the Document which is been editing */
	private Document doc;
	/** The page currently display */
	private int nbPage;
	/** the Sequence in the document which is editing by the client */
	private Sequence currentEditingSequence; 
	/** to communicate with all participants of the current session */
	private SharedEdition sharedEdition;
	
	
	/**
	 * Create a thread to communicate with a client through a TCP socket.
	 * @param socket
	 * @throws IOException if we can't open sockets streams
	 */
	public ThreadClientTCP(Socket socket) throws IOException {
		ID = InstanceCounter++;
		
		/** create a model for the client */
		client = new ClientTCP(socket);
	}

	@Override
	public void run() {
		
		/* -- First it sends the connection panel to the client -- */
		try {	
			this.client.sendMessage(new ConnectionPanel());
			System.err.println("Paneau de connexion envoyé.");	
			
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Sending conection page error. The socket is closing for the client "+ID);
			this.interrupt();
			return;
		}
		
		
		/* -- Then it waits for a message until it is interrupted by the server -- */
		// treatment :
		JSONObject messageJSON;
		String type;
		// response :
		Object response = null;
		
		
		while(!isInterrupted()) {
			try {
				System.out.println("wait for a message... : ");
				
				/** wait for a request from the client */
				messageJSON = this.client.waitForRequest();
				
				/* -- Next it identify the object of the message and make the response in consequence -- */
				type = (String)messageJSON.get("type");
				
				if(type.equals("connexion")) 
				{
					System.out.println("A client ask for connexion");
					// TODO : chercher l'util dans la BDD.
					this.client.setId(0);
					this.client.setMail("unknow");
					this.client.setPseudo("unknow");
					
					/* -- register him to the mainframe server --*/
					MainframeServer.instance.addClient(this.client);
					
					this.client.sendMessage(new HomePanel());
				}
				else if(type.equals("request")) 
				{
					String value = (String)messageJSON.get("value");
					
					if(value.equals("newDocument"))
					{
						System.out.println("New document request");

						this.doc = new Document(this.client);
						this.nbPage = doc.createPage();
						/*this.currentEditingSequence =*/ 
						//doc.getPage(nbPage).createNewSequence(client, SequenceType.SequenceText, 0);

						this.client.sendMessage(new DocumentPanel()); // send JPanel
						this.client.sendMessage(doc); // send document to edit
						editionMode();
						
						// redirection to home
						this.client.sendMessage(new HomePanel());
					}
					else if (value.equals("joinEdition")) 
					{
						// TODO : récup l'édition partagé, vérif que != null, envoyer le panel + le doc
						System.out.println("JoinEdition request.");
						sharedEdition = MainframeServer.instance
								.getsharedEdition(
										Integer.parseInt((String)messageJSON.get("id")), 
										(String)messageJSON.get("password"),
										client);

						if(sharedEdition != null) {
							sharedEdition.join(client); // register this client in this session
							System.out.println("Send a Jpanel + Document Session");
							this.client.sendMessage(Integer.valueOf(sharedEdition.ID));
							this.client.sendMessage(new DocumentPanel(sharedEdition.ID)); // send JPanel
							doc = sharedEdition.getDoc();
							this.client.sendMessage(this.doc); // send document to edit
							editionMode();

							// redirection to home
							this.client.sendMessage(new HomePanel());
						}
						else {
							//this.client.sendMessage(Integer.valueOf(-1));	
							this.client.sendMessage("NO");
						}
					}
				}
				else 
				{
					System.out.println("client n° "+ID+"received "+messageJSON);
					response = null;
				}
				
				
				/* -- Finally it sends the response to the client -- */
				if(response != null) {
					this.client.sendMessage(response);
					response = null;
				}
								
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}			
		}
		
		System.out.println("End of the Client thread.");
	}
	
	
	
	@Override
	public void interrupt() {
		super.interrupt();
		this.client.close();
	}
	
	
	
	/**
	 * This method is called when the client of the current thread
	 * edit a document.
	 */
	@SuppressWarnings("unchecked")
	private void editionMode() {
		System.out.println("Entering in edition mode.");
		boolean keepWaitingForCommands = true;
		Object o = null;
		Command command;

		
		do {
			try {
				o = client.receiveMessage();
			} catch (ClassNotFoundException | IOException e) {
				
				e.printStackTrace();
			} 
			// get a command from the client
			if(o instanceof Command) {
				command = (Command)o;
				
				/* --  add the pointer to the sequence to the command-- */
				System.err.println("Server command received page "+command.getPageNumber()+" seq "+command.getSequenceNumber());
				command.setSequence(doc.getPage(command.getPageNumber()).getSequence(command.getSequenceNumber()));
				
				/* -- add it to the document to be executed -- */
				doc.editWhithThisCommand(command);
				//System.out.println("A command has been executed on the server side.");
				//System.out.println(currentEditingSequence.toString());
				
				/* -- send the command to all client if he has been made by the client, so they can see the change -- */
				if(sharedEdition != null  /*&&  currentEditingSequence == command.getSequence()*/)
				{
					for(Client client : sharedEdition.getClients()) {
						if(client == this.client)
							; // he send the command and don't need it
						else {
							System.out.println("SendModification");
							try {
								client.sendModification(command);
							} catch (IOException e) {
								System.err.println("Command couldn't be send.");
								e.printStackTrace();
							}
						}
					}
				}
				
			}
			else if(o instanceof JSONObject) {
				JSONObject request = (JSONObject)o;
				
				RequestType type = RequestType.valueOf((String)request.get("TYPE"));
				
				switch(type) {
				
				case REQUEST_CREATE_SHARE_EDITION:
					// ask for creating a session
					sharedEdition = MainframeServer.instance.createSharedEdition(doc, (String)request.get("value"), client);
					try {
						client.sendMessage(sharedEdition.ID);
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("Error while responding to a create sharedEdition request");
					}
					break;
					
				case REQUEST_QUIT:
					keepWaitingForCommands = false;
					break;
					
				case REQUEST_RELEASE_EDITION:
					break;
					
				case REQUEST_TAKE_EDITION:
					String response;
					int nbPage = (int) request.get("PAGE");
					int nbSequence = (int) request.get("value");
					Sequence sequenceWantToModify = this.doc.getPage(nbPage).getSequenceToModify(client, nbSequence);
					System.out.println("demande d'édition p="+nbPage+" s="+nbSequence);

					if(sequenceWantToModify != null) { // allow to modify 
						// free the current sequence and add the new one
						if(this.currentEditingSequence != null) {
							this.currentEditingSequence.releaseEdition(client);
						}

						this.currentEditingSequence = sequenceWantToModify;
						System.out.println("edition de sequence changé p="+nbPage+" s="+nbSequence);
						response = "T";
						System.out.println("envoie T");
					}
					else { // reject
						response = "F";
						System.out.println("envoie F");
					}
					
					// send the response to the client
					try {
						client.sendMessage(response);
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					break;
					
				case REQUEST_CREATE_SEQUENCE_TEXT:
					JSONObject message = new JSONObject();
					Sequence newSequence = doc.getPage(this.nbPage).createNewSequence(client, SequenceType.SequenceText);
					
					// TODO : remove debug lines
					if(newSequence == null) {
						System.err.println("THreadClientTCP error new Sequence null");
					}
					else {
						System.out.println("threadClientTCP new sequence créée et envoyée "+newSequence);
					}
					
					// send the new sequence to the client
					try 
					{
						message.put("SEQUENCE", newSequence);
						message.put("EDIT", true);
						//client.sendMessage(newSequence);
						client.sendMessage(message);
						
					} catch (IOException e) {
						System.err.println("La nouvelle séquence demandé n'a pu être envoyé.");
						e.printStackTrace();
					}
					
					// if the doc is shared we need to notify each client
					if(this.sharedEdition != null) {
						// TODO : make a method for a broadcast.
						message.put("EDIT", false);
						for(Client c : this.sharedEdition.getClients()) {
							if(c == this.client)
								;// already send the message
							else {
								try {
									c.sendModification(message);
								} catch (IOException e) {
									e.printStackTrace();
									System.err.println("Erreur de l'envoie de la nouvelle séquence pour le client "+client.getId());
								}
							}
						}
					}
					
					break;
					
				default:
					break;
				
				}
			}
			// for backward compatibility ...
			else if(o instanceof String){ // TODO delete this because it was made before the new JSON protocol which is better.
				String request = (String)o;
				System.err.println("ThreadClientTCP recieve a message using a deprecate protocol. Modify it!");
				
				if(o.equals("STOP"))keepWaitingForCommands = false;
				else {
					// ask for creating a session
					sharedEdition = MainframeServer.instance.createSharedEdition(doc, request, client);
					try {
						client.sendMessage(sharedEdition.ID);
					} catch (IOException e) {
						e.printStackTrace();
						System.out.println("Error while responding to a create sharedEdition request");
					}
				}
			}

		}while(keepWaitingForCommands );
		System.out.println(" !!! OUT OF EDITION MODE !!! ");

	}

	
	/* -- generated get and set methods -- */


	/**
	 * @return the nbPage
	 */
	public int getNbPage() {
		return nbPage;
	}

}