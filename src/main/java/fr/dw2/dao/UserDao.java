package fr.dw2.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.dw2.models.user.User;

/**
 * This class makes the connection to the database in the 
 * user table
 * @author manuela
 *
 */
public class UserDao {
	private PreparedStatement ins;
	private PreparedStatement sel;
	private PreparedStatement sel2;
	private Connection c;

	public UserDao() {
		// load JDBC pilote
		try {
		Class.forName("com.mysql.jdbc.Driver");
		}
		catch(ClassNotFoundException e) {}
				
		// registration on driver manager
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// connection to the database
		try {
			c = DriverManager.getConnection("jdbc:mysql://localhost:3305/users", "root", "");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		// statement zone
		try {
			sel = c.prepareStatement("SELECT name,email " + "FROM users "+ " WHERE password = ?");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ins = c.prepareStatement("INSERT INTO users(name,email,password) " + "VALUES (?,?,?)");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			sel2 = c.prepareStatement("SELECT ? " + "FROM users");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 /* *
	 * Check if the user is registered in the database
	 * @param id - identifier of the user
	 * @param password - password of the user
	 * @return true if a user is registered in the database and false if not
	 */
	public boolean verifyUser(User u) throws SQLException,ClassNotFoundException {
		sel.setString(1, u.getPwd());
		ResultSet res = sel.executeQuery();
		boolean registered = false;
		while(!registered & res.next()) {
			if(res.getString("name").compareTo(u.getName()) == 0 || res.getString("email").compareTo(u.getEmail()) == 0)
				registered = true;
		}
		return registered;
	}
	
	/**
	 * Insert a new user in the database
	 * @param name
	 * @param email
	 * @param pwd
	 * @return string - message if the registration succeed
	 * @throws SQLException
	 */
	public String insertUser(User u) throws SQLException, ClassNotFoundException {
		ResultSet res;
		sel2.setString(1, u.getName());
		res = sel2.executeQuery();
		if(res.next())
			return "Nom déjà utilisé, choisissez-en un autre";
		sel2.setString(1, u.getEmail());
		res = sel2.executeQuery();
		if(res.next())
			return "Email déjà utilisé, choisissez-en un autre";
		ins.setString(1, u.getName());
		ins.setString(2, u.getEmail());
		ins.setString(3, u.getPwd());
		ins.executeUpdate();
		return "OK";
	}
	
	/**
	 * Close the connection
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		ins.close();
		sel.close();
		sel2.close();
		c.close();
	}
}
