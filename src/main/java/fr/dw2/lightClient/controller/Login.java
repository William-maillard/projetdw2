package fr.dw2.lightClient.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.dw2.controllers.server.MainframeServer;
import fr.dw2.dao.UserDao;
import fr.dw2.models.Client;
import fr.dw2.models.ClientServlet;
import fr.dw2.models.user.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login/")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// if method="get", redirect to connection page
		request.setAttribute("path", "Login");
		getServletContext().getRequestDispatcher("/WEB-INF/views/connect.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// register the id and the password in variables for verification in the database
		String id = request.getParameter("id");
		String pwd = request.getParameter("pwd");
		
		// remove leading and ending space in the password and id
		id = id.trim();
		pwd = pwd.trim();
		if(id.length()>0 & pwd.length()>0) {
			request.setAttribute("id", id);
			request.setAttribute("pwd", pwd);
			
			UserDao uDao = new UserDao();
			User user = new User(id,id,pwd);
			// checks in the database if the user exists
			ClientServlet client = new ClientServlet(request.getSession());
			client.setId(0);
			client.setMail("unknow");
			client.setPseudo("unknow");
			/* -- register the client to th mainFrameServer and add it to the session -- */
			MainframeServer.instance.addClient(client);
			request.getSession().setAttribute("client", client);
			
			try {
				if(uDao.verifyUser(user))
					getServletContext().getRequestDispatcher("/WEB-INF/views/accueil.jsp").forward(request, response);
				else
					getServletContext().getRequestDispatcher("/WEB-INF/views/connect.jsp").forward(request, response);
			} catch (ClassNotFoundException | SQLException | ServletException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// redirects to the connection page
		else {
			request.setAttribute("error", "Erreur, vous devez remplir tout le formulaire");
			getServletContext().getRequestDispatcher("/WEB-INF/views/connect.jsp").forward(request, response);
		}
		
	}

}
