package fr.dw2.lightClient.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import fr.dw2.dao.UserDao;
import fr.dw2.models.user.User;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register/")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// if method="get", redirect to registration page
		request.setAttribute("path", "Register");
		getServletContext().getRequestDispatcher("/WEB-INF/views/register.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// register the id and the password in variables for verification in the database
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String pwd = request.getParameter("pwd");
		
		if((name.trim().length()>0 || email.trim().length()>0) & pwd.trim().length()>0) {
				request.setAttribute("name", name);
				request.setAttribute("email", email);
				request.setAttribute("pwd", pwd);
				
				// message to signal successful registration
				UserDao uDao = new UserDao();
				User user = new User(name, email, pwd);
				try {
					String msg = uDao.insertUser(user);
					if(msg.compareToIgnoreCase("OK") == 0)
						getServletContext().getRequestDispatcher("/WEB-INF/views/connect.jsp").forward(request, response);
					else {
						request.setAttribute("error", "Erreur, nom et/ou email déjà utilisé");
						getServletContext().getRequestDispatcher("/WEB-INF/views/register.jsp").forward(request, response);
					}
						
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
			// redirects to the registration page
			else {
				request.setAttribute("error", "Erreur, le nom ou mot de passe et le mot de passe ne doivent pas contenir d'espaces");
				getServletContext().getRequestDispatcher("/WEB-INF/views/register.jsp").forward(request, response);
			}
	}

}
