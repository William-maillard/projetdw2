<jsp:include page="header.jsp">
	<jsp:param value="Inscription" name="title"/>
</jsp:include>

<div class="container">
	<form action="${path}" method="POST">
		<div class="item">
			<input type="text" name="name" id="name" placeholder="Nom"/>
			<br/>
		</div>
		<div class="item">
			<input type="email" name="email" id="email" placeholder="Email"/>
			<br/>
		</div>
		<div class="item">
			<input type="password" name="pwd" id="pwd" placeholder="Mot de passe"/>
			<br/>
		</div>
		<button type="Submit">Inscription</button>
	</form>
</div>
<jsp:include page="footer.jsp"></jsp:include>
