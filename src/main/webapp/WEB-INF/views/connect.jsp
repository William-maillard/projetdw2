<jsp:include page="header.jsp">
	<jsp:param value="Connexion" name="title"/>
</jsp:include>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setBundle basename="fr.dw2.bundles.bundle"/>

	<c:if test="${not empty error}">
		<p>${error}</p>
	</c:if>
	
<div class="container">
	<form action="" method="POST">
		<div class="item">
			<fmt:message var="nom" key="label.name"></fmt:message>
			<fmt:message var="mail" key="label.mail"></fmt:message>
			<input type="text" name="id" id="id" required placeholder="${nom}, ${mail }"/> <span>*</span>
			<br/>
		</div>
		<div class="item">
			<fmt:message var="mdp" key="label.pwd"></fmt:message>
			<input type="password" name="pwd" id="pwd" required placeholder="${mdp }"/> <span>*</span>
			<br/>
		</div>
		<button type="Submit"><fmt:message key="label.connect"/></button>
	</form>
	<div class="separator"></div>
	<div class="block">Vous n'avez pas de compte ? <a href="/text-editor/Register">Inscrivez-vous</a></div>
</div>

<jsp:include page="footer.jsp"></jsp:include>