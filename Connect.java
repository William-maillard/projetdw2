package fr.dw2.server;


import org.springframework.security.crypto.bcrypt.BCrypt;

public class Connect {
	private PreparedStatement insUser;
    private PreparedStatement insDoc;
    private PreparedStatement insRights;
	private PreparedStatement sel;
	private Connection c;
	
	/**
	 * Constructor
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws java.io.IOException
	 */
	public Connect() throws SQLException, ClassNotFoundException, java.io.IOException {
		// load JDBC pilote
		Class.forName("com.mysql.jdbc.Driver");
		
		// registration on driver manager
		DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		
		// connection to the database
		c = DriverManager.getConnection("jdbc:mysql://localhost:5432/", "db_web", "password");
	
		// statement zone
		sel = c.prepareStatement("SELECT ?,? " + " FROM ? "+ " WHERE password = ?");
		insUser = c.prepareStatement("INSERT INTO users(nickname, email, password) " + "VALUES(?,?,?)");
        insDoc = c.prepareStatement("INSERT INTO documents(idAuthor, idGroup, title) " + "VALUES(?,?,?)");
        insRights = c.prepareStatement("INSERT INTO rights(idDocument, idUser, rights) " + "VALUES(?,?,?)");
	}
	
	/**
	 * 
	 * @param nickname - identifier of the user
     * @param email - email of the user
	 * @param password - password of the user
	 * @return true if a user is registered in the database and false if not
	 */
	public boolean verifyUser(String nickname, String email, String password) {
		return true;
	}
	
    public void addUser(String nickname, String email, String password) throws SQLException  {

		String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        
        insUser.setString(1, nickname);
        insUser.setString(2, email);
        insUser.setString(3, hashedPassword);
        insUser.executeUpdate();
	}

    public void addDocument(String idAuthor, String idGroup, String title) throws SQLException  {
        
        insDoc.setString(1, idAuthor);
        insDoc.setString(2, idGroup);
        insDoc.setString(3, title);
        insDoc.executeUpdate();
	}

    public void addRights(int idDocument, int idUser, int rights) throws SQLException  {
        
        insRights.setInt(1, idDocument);
        insRights.setInt(2, idUser);
        insRights.setInt(3, rights);
        insRights.executeUpdate();
	}

	/**
	 * Close the connection
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		insUser.close();
        insDoc.close();
        insRights.close();
		sel.close();
		c.close();
	}
}
